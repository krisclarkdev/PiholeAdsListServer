# PiholeAdsListServer

Manage your [pihole](https://pi-hole.net/) ad lists via [configmap](https://kubernetes.io/docs/concepts/configuration/configmap/) and serve them to pihole using [nginx](https://www.nginx.com/) on [Kubenrnetes](https://kubernetes.io/)

# How to use

Clone 

```
https://gitlab.com/krisclarkdev/PiholeAdsListServer.git
cd PiholeAdsListServer
```

Edit the ads list

```
vi adslist1.yaml
```

Create the namespace

```
kubectl create namespace dns
```

Create the ads list

```
kubectl create -f ./adslist1.yaml
```

Edit the deployment to match your enviornment.  Kubernetes imposes a default max file size for configmaps.  So I'd recommend you keep each ad list under 18,000 domains.

Deploying with one ad list

```
kubectl create -f ./PiholeAdsListServer.yaml
```

Deploying with multiple ad lists

Create a new ad list

```
$ touch adlist2.yaml
$ vi adlist2.yaml

apiVersion: v1
kind: ConfigMap
metadata:
  name: adlist-configmap-2
  namespace: dns
data:
  # file-like keys
  ads2.txt: |
    0.0.0.0 eu1.clevertap-prod.com
    0.0.0.0 wizhumpgyros.com
    0.0.0.0 coccyxwickimp.com
    0.0.0.0 webmail-who-int.000webhostapp.com
```

Then edit the deployment to match it would look something like this

```
          - name: nginx-ads-file-1
            mountPath: /usr/share/nginx/html/ads1.txt
            subPath: ads1.txt
          - name: nginx-ads-file-2
            mountPath: /usr/share/nginx/html/ads2.txt
            subPath: ads2.txt

        - name: nginx-ads-file-1
          configMap:
            name: adlist-configmap-1
        - name: nginx-ads-file-2
          configMap:
            name: adlist-configmap-2
```

Then deploy

```
kubectl create -f ./PiholeAdsListServer.yaml
```

Finally use an [ingress](https://kubernetes.io/docs/concepts/services-networking/ingress/) of your choice and you can then point [pihole](https://pi-hole.net/)  to nginx:80/ads1.txt, nginx:80/ads2.txt, and so on - or if your PiHole is running on Kubernetes and you want to be extra secure http://adlists.dns.svc.cluster.local:80/ads1.txt....
